## 1、Cmake编译安装

```
sudo apt install g++
sudo apt install make
sudo apt install ninja-build
sudo apt install libssl-dev
wget https://github.com/Kitware/CMake/releases/download/v3.30.5/cmake-3.30.5.tar.gz
tar -xvf cmake-3.30.5.tar.gz
cd cmake-3.30.5

// 生成makefile -j多线程
./configure
make -j32
// 安装
sudo make install
cmake --version
```

window版 设置cmake bin目录的环境变量

```
// .当前目录下查找CMakeList.txt
// cd build
// cmake ..
// 当前目录下找CMakeList.txt 编译到build目录下
cmake -S . -B build
// 默认编译debug 安装release，这里修改下
cmake --build build --config Release
// 安装
cmake --install build
```

## 2、Windows编译首个cpp

first.cpp内容

```
#include <iostream>
using namespace std;

int main(int argc, char* argv[]) {
	cout << "hello cmake" << endl;
	return 0;
}
```

CMakeLists.txt内容

```
cmake_minimum_required(VERSION 3.17)

project(first)

# 构建执行程序 程序名 文件名
add_executable(first first.cpp)
```

编译

```
// 进入项目当前目录 编译项目文件到build目录
cmake -S . -B build
// 编译可执行文件 编译build目录下 Windows指定Release
cmake --build build --config Release
```

## 3、Ubuntu中编译首个cpp

生成项目文件：

- 默认makefile  cmake -S . -B build
- 指定Ninja       cmake -S. -B build -G "Ninja"

windows不支持在目录下使用仅使用cmake命令，linux下可以在build目录下直接cmake

```
// 编译当前目录文件到build中
cmake -S . -B build
// 项目文件编译
// 进入build文件 则cmake即可
cmake --build build
```

## 4、编译xlog静态库

linux下

```
xlog_lib
	xlog
		xlog.h
		xlog.cpp
		build
			libxlog.a
	test_xlog
		test_xlog.cpp

mkdir xlog_lib
cd cmake_lib
mkdir xlog
mkdir test_xlog
cd xlog
vim xlog.h

	// xlog.h
	#ifndef XLOG_H
	#define XLOG_H
	class XLog {
	public:
		XLog();	
	};
	#endif
	
vim xlog.cpp

	// xlog.cpp
	#include "xlog.h"
	#include <iostream>
	using namespace std;
	XLog::XLog() {
		cout << "xlog lib" << endl;
	};
	
vim CMakeLists.txt
cmake_minimum_required(VERSION 3.17)
project(xlog)
add_library(xlog STATIC xlog.cpp xlog.h)

cmake -S . -B build
cmake --build build
```

windows下

```
cmake -S . -B build
// debug
cmake --build build
// release
cmake --build build --config Release
```

## 5、链接静态库

```
xlog_lib
	xlog
		xlog.h
		xlog.cpp
		build
			libxlog.a
	test_xlog
		test_xlog.cpp

// test_xlog.cpp
#include <iostream>
#include "xlog.h"
using namespace std;
int main() {
	XLog log;
	cout << "test log" << endl;
	return 0;
}

#CMakeLists.txt 
cmake_minimum_required(VERSION 3.17)
project(test_xlog)
# 1. 指定头文件路径
include_directories("../xlog")
# 2. 查找库路径 windows下会在Debug、Release中自动找
link_directories("../xlog/build")
add_executable(test_xlog test_xlog.cpp)
# 3. 指定加载的库
target_link_libraries(test_xlog xlog)
```

## 6、编译动态库

linux下编译

```
xlog_lib
	xlog
		xlog.h
		xlog.cpp
		CMakeLists.txt
	test_xlog
		test_xlog.cpp
		CMakeLists.txt

基于上面使用的静态库的路径编译动态库

在xlog_lib这层使用一个CMakeLists.txt
cmake_minimum_required(VERSION 3.17)
project(xlog)
# 头文件路径
include_directories("xlog")
# 编译动态库
add_library(xlog SHARED xlog/xlog.cpp xlog/xlog.h)
add_executable(test_xlog test_xlog/test_xlog.cpp)
# 添加库链接
target_link_libraries(test_xlog xlog)

# 可以查看动态库链接关系
ldd test_xlog
# Cmake自动会加绝对路径
# 使用GCC编译时需要手动添加，命令如下： -Wl,-rpath,xxx  xxx就是绝对路径
```

windows下编译

同样的命令，windows下只能生成dll文件（函数二进制代码），没有生成lib文件（函数地址索引）

所以需要修改库头文件

```
	// xlog.h
	#ifndef XLOG_H
	#define XLOG_H
	
	#ifdef xlog_EXPORTS
	// 项目调用时使用dllimport
	class __delspec(dllimport) XLog {
	#else
	// 库调用时使用dllexport __delspec(dllexport) 双下划线+export 导出XLog类的函数到lib文件中
	class __delspec(dllexport) XLog {
	#endif
	public:
		XLog();	
	};
	#endif
	
# 编译动态库 自带预处理变量 xlog_EXPORTS
add_library(xlog SHARED xlog/xlog.cpp xlog/xlog.h)	

为了让头文件再兼容linux
	// xlog.h
	#ifndef XLOG_H
	#define XLOG_H
	#ifndef _WIN32
		// linux 使用空
		#define XCPP_API
	#else
		#ifdef xlog_EXPORTS
			// 项目调用时使用dllimport
			#define XCPP_API __delspec(dllimport)
		#else
			// 库调用时使用dllexport 
			#define XCPP_API __delspec(dllexport)
		#endif
	#endif
	class XCPP_API XLog {
	public:
		XLog();	
	};
	#endif
```

## 7、日志

```
# 行注释
#[[
多行注释
]]
cmake_minimum_required(VERSION 3.17)
project(message)
message("参数1")
message("参数1" "参数2" "参数3")
message("参数1" #[[注释加在中间]] "参数2" "参数3")
```

指定日志级别

```
message(FATAL_ERROR "进程退出 生成退出")

message(SEND_ERROR "进程继续" 生成退出)
# 不会生成项目 add_library add_executable 不会执行
add_executable(test_message test_message.cpp)

message(WARNING "警告")
message(NOTICE "注意 不会打印行号") # 什么都不加时就是这个级别
message(STATUS "状态 会加前缀 -- ")
message(VERBOSE "详细 加前缀 -- 默认不显示") #显示需要修改参数 cmake -S . -B build --log-level=VERBOSE

message(DEBUG "前缀 -- debug")
message(TRACE "前缀 -- 跟踪")

# 重定向到txt 标准输出只会 STATUS VERBOSE
cmake -S . -B build --log-level=VERBOSE > log.txt
# 默认只有 标准输出1  标准错误输出2 需要再加配置 2>&1 2输出到1中
cmake -S . -B build --log-level=VERBOSE > log.txt 2>&1
```

查找库

```
# 开始查找
message(CHECK_START "find xcpp")
# 查找库的代码

# 嵌套查找
message(CHECK_START "find xlog")
message(CHECK_PASS "found")

# 结束查找
message(CHECK_FAIL "not found")
```

设置日志缩进

```
# 设置
set(CMAKE_MESSAGE_INDENT "--")
# 取消
set(CMAKE_MESSAGE_INDENT "")
```

输出不同的颜色

```
开头Esc ASCII27   [  颜色  m
\033[1;31;40m
0 默认设置
1 高亮显示
4 下划线
5 闪烁
7 反白显示
8 不可见

前景色 30 - 37
	黑 红 绿 黄 蓝 紫红 青蓝 白
背景色 40 - 47
	黑 红 绿 黄 蓝 紫红 青蓝 白
	
string(ASCII 27 Esc)
set(R "${Esc}[0;31m") # 红色
set(RB "${Esc}[0;31;40m") # 蓝色字体 黑色背景
set(B "${Esc}[1;34m") # 蓝色高亮
set(E "${Esc}[m")
message("${R}红色内容${E}")
message("${R}蓝色内容${E}")
```



## 8、变量

```
cmake_minimum_required(VERSION 2.17)
project(test_ver)
set(VAR1 "测试变量")
message("VAR1=" ${VAR1})
message("VAR1=${VAR1}")
# 清除后返回空串
unset(VAR1)
```

**cmake自带变量**

使用上面的lib示例，目录结构如下

```
xlog_lib
	xlog
		xlog.h
		xlog.cpp
		CMakeLists.txt
```

CMakeLists.txt

```
cmake_minimum_required(VERSION 3.17)
project(xlog)
# 提供信息的变量 ${PROJECT_NAME}
# 改变行为的变量 BUILD_SHARED_LIB ON 动态库 OFF 静态库 默认OFF
set(BUILD_SHARED_LIBS OFF)

# cmake 传递变量给 c++
add_definitions(-Dxlog_STATIC) # 传递xlog_STATIC 默认1

# 描述系统的变量
message("MSVC = " ${MSVC})
message("WIN32 = " ${WIN32})
message("UNIX = " ${UNIX})
message("CMAKE_SYSTEM_NAME = " ${CMAKE_SYSTEM_NAME})
# 控制构建过程的变量 是否生成makefile的颜色 默认on
set(CMAKE_COLOR_MAKEFILE OFF)
add_library(${PROJECT_NAME} xlog.cpp xlog.h)
```

让头文件兼容静态库

```
	// xlog.h
	#ifndef XLOG_H
	#define XLOG_H
	#ifndef _WIN32
		// linux 使用空
		#define XCPP_API
	#else
		// 再自定义一个宏表示编译静态库
		#ifdef xlog_STATIC
			#define XCPP_API
         #else
         	// cmake自带的宏
            #ifdef xlog_EXPORTS
                // 项目调用时使用dllimport
                #define XCPP_API __delspec(dllimport)
            #else
                // 库调用时使用dllexport 
                #define XCPP_API __delspec(dllexport)
            #endif
         #endif    
	#endif
	class XCPP_API XLog {
	public:
		XLog();	
	};
	#endif
```
